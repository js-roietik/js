function range(start, end, step) 
{
        var array = [];  
        if (start < end) 
        {
            if ((arguments.length < 3) || step <= 0) { step = 1; }
            for (var i = start; i <= end; i += step) {
                array.push(i);
        }
    } 
    else 
    {
        if ((arguments.length < 3) || step >= 0) { step = -1; }
        for (var j = start; j >= end; j += step) 
        {
            array.push(j);
        }
    }
    return array;
}

function sum(array) 
{
    var total = 0;
    for (var i = 0, len = array.length; i < len; i++) 
    {
        total += array[i];
    }
    return total;
}

console.log(sum(range(1,10)));
console.log(range(5,2,-1));
