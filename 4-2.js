var reverseArray = function(space) {
    var newArr = [];
    for (var i = space.length - 1; i >= 0; i--)
        newArr.push(space[i]);
    return newArr;
};

console.log(reverseArray([1,2,3,4,5]));

var reverseArrayInPlace = function(space) {
    var temp = 0;
    for (var i = 0; i < space.length / 2; i++) {
        temp = space[i];
        space[i] = space[space.length - i - 1];
        space[space.length - i - 1] = temp;
    }
};
var arrayValue = [1, 2, 3, 4, 5, 6, 7, 8, 9];
var arrayValue2 = [1, 2, 3, 4, 5, 6];
reverseArrayInPlace(arrayValue2);
console.log(arrayValue2);